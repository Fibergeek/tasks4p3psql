import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    with open(os.path.join(os.path.dirname(__file__), fname)) as f:
        return f.read()

setup(
    name = 'tasks4p3psql',
    version = '0.0.5',
    author = 'Peter Lemmens',
    author_email = 'fibergeek@gmail.com',
    description = 'An task (worker) library that uses PostgreSQL as broker.',
    license = 'BSD',
    packages=['tasks4p3psql'],
    long_description=read('README.md'),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Topic :: Utilities',
        'License :: OSI Approved :: BSD License',
    ],
)
