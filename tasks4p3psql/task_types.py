import base64

import dill

class Task:
  def __init__(self, type: str, input: object):
    self.task_id = None
    self.input = input
    self.payload = {}
    self.payload['type'] = type
    self.payload['args'] = input
    self.is_finished = False
    self.result = None
    self.stdout = None
    self.stderr = None
    self.timeout = 10
    self.requirements = {}

class EvalTask(Task):
  def __init__(self, expression: str):
    Task.__init__(self, 'python_eval', expression)

class SafeFunctionTask(Task):
  def __init__(self, function):
    Task.__init__(self, 'python_func', function)
    self.payload['func'] = base64.b85encode(dill.dumps(function)).decode('ascii')
    del self.payload['args']

class FunctionTask(Task):
  def __init__(self, function):
    Task.__init__(self, 'python_isolated', function)
    self.payload['func'] = base64.b85encode(dill.dumps(function)).decode('ascii')
    del self.payload['args']

class ShellTask(Task):
  def __init__(self, cmd: str):
    Task.__init__(self, 'shell_cmd', cmd)

def WithTimeout(task: Task, timeout: int):
    task.timeout = timeout
    return task

def WithRequirements(task: Task, requirements: dict):
    task.requirements.update(requirements)
    return task

