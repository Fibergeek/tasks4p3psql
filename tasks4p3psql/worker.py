import os
import ssl
import sys
import json
import uuid
import base64
import ctypes
import asyncio
import getpass
import platform

import dill
import asyncpg

try:
  import psutil
  CPU_COUNT = psutil.cpu_count(logical=False)
except ImportError:
  CPU_COUNT = os.cpu_count()

WORKER_VERSION = '0.2.0'
CONNECTION_DETAILS = {'user':'postgres', 'password':'', 'database':'tasks', 'host':'localhost', 'port': 5432}
PRINT_SUBPROCESS_RESULTS = False

__pg_lock = asyncio.Lock()
__main_lock = asyncio.Lock()
__threads = [None] * CPU_COUNT
__worker_ns = 'https://%s/%s' % (getpass.getuser(), uuid.getnode())
__worker_id = uuid.uuid5(uuid.NAMESPACE_URL, __worker_ns)

def main(args=None):
    if args != None:
        configure_globals(args)
    
    print('Worker version:', WORKER_VERSION, 'Available CPU\'s:', len(__threads))
    loop = asyncio.get_event_loop()
    task = loop.create_task(asyncpg_listen(CONNECTION_DETAILS, 'new_task', on_notification_handler, on_connection_handler))
    try:
        loop.run_until_complete(task)
    except KeyboardInterrupt:
        print()
    finally:
        task.cancel()
    return 0

def test(args):
    configure_globals(args)
    print('Worker version:', WORKER_VERSION, 'Available CPU\'s:', len(__threads))
    loop = asyncio.get_event_loop()
    task = loop.create_task(test_connection(CONNECTION_DETAILS))
    try:
        loop.run_until_complete(task)
        print('Connection OK')
    except Exception as ex:
        print(ex)
    finally:
        task.cancel()
    return 0

def print_info(args):
    global __worker_id
    
    configure_global_worker_id(args)
    print('Worker VS:', WORKER_VERSION)
    print('Worker NS:', __worker_ns)
    print('Worker ID:', __worker_id)

def configure_globals(args):
    configure_global_worker_id(args)
    configure_global_connection_details(args)

def configure_global_worker_id(args):
    global __worker_id, __threads

    if args.wid:    
        __worker_id = as_uuid(args.wid)
    if args.cpu_count > 0:
        __threads.clear()
        __threads.extend([None] * args.cpu_count)
    elif args.cpu_count > 0:
        raise ValueError('CPU Count cannot be negative!')

def configure_global_connection_details(args):
    global CONNECTION_DETAILS
    
    CONNECTION_DETAILS['host'] = args.host
    CONNECTION_DETAILS['port'] = args.port
    CONNECTION_DETAILS['user'] = args.username
    CONNECTION_DETAILS['password'] = args.password
    CONNECTION_DETAILS['database'] = args.dbname
    
    if args.sslcrt or args.sslkey:
       ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
       ctx.load_cert_chain(certfile=args.sslcrt, keyfile=args.sslkey)
       CONNECTION_DETAILS['ssl'] = ctx

def as_uuid(val):
    return uuid.UUID(str(val))

async def asyncpg_listen(
    connection_details, channel, on_notification_handler, on_connection_handler=None, *, conn_check_interval=60, conn_check_timeout=5, reconnect_delay=1):
  conn = None
  try:    
    while True:
        try:
            conn = await asyncpg.connect(**connection_details)
            await conn.add_listener(channel, on_notification_handler)
            
            if on_connection_handler is not None:
                await on_connection_handler(conn)
            
            while True:
                await asyncio.sleep(conn_check_interval)
                async with __pg_lock:
                  await conn.execute('SELECT register_worker($1::uuid)', __worker_id, timeout=conn_check_timeout)
        except asyncio.CancelledError:
            raise
        except:
            # Probably lost connection
            pass
        
        if reconnect_delay > 0:
            await asyncio.sleep(reconnect_delay)
  finally:
    if conn != None:
      try:
        await conn.close()
      except:
        pass

async def test_connection(connection_details):
   conn = await asyncpg.connect(**connection_details)
   await conn.close()

def on_notification_handler(conn, pid, channel, payload):
    asyncio.create_task(fetch_a_task(conn))

async def on_connection_handler(conn):
    await conn.execute('SELECT register_worker($1::uuid)', __worker_id)
    await fetch_a_task(conn)

async def fetch_a_task(conn):
   async with __main_lock:
      for i, t in enumerate(__threads):
        if t == None:
          async with __pg_lock:
            update_key = await conn.fetchval('SELECT get_available_task($1::uuid, $2)', __worker_id, get_platform_id())
          if update_key:
            __threads[i] = asyncio.create_task(execute_task(i, conn, update_key))
            print('Assigned an update key to slot', i)
          else:
            print('No task was waiting for slot', i)
            break

async def execute_task(task_index, conn, update_key):
    try:
      async with __pg_lock:
        fetch_result = (await conn.fetchrow('SELECT payload, requirements FROM tasks WHERE update_key=$1', update_key)) or {}
      payload = json.loads(fetch_result.get('payload'))
      requirements = json.loads(fetch_result.get('requirements') or '{}')
      
      cmd, func, args, stdin = None, str(payload.get('func', '')), str(payload.get('args', '')), str(payload.get('stdin', ''))
      result, result_exception = None, None
      errors = test_requirements(requirements)
      if not errors:
        try:
            stdin = base64.b85decode(stdin)
        except Exception as ex:
            errors.append('Unsupported stdin data!')
        
        if isinstance(payload, dict) and payload.get('type') == 'shell_cmd':
            cmd = args
            print('Running shell command:', cmd)
        elif isinstance(payload, dict) and payload.get('type') == 'python_shell':
            cmd = sys.executable + ' ' +  args
            print('Running python command:', cmd)
        elif isinstance(payload, dict) and payload.get('type') == 'python_eval':
            if stdin:
                errors.append('stdin is not supported for this payload type!')
            try:
                print('Running python eval: eval(', repr(args), ')')
                result = eval(args)
            except Exception as ex:
                result_exception = ex
        elif isinstance(payload, dict) and payload.get('type') == 'python_func':
            if stdin:
                errors.append('stdin is not supported for this payload type!')
            try:
                func = base64.b85decode(func)
                args = base64.b85decode(args)
                print('Running python marshal:', args, '()')
                func = dill.loads(func)
                if args:
                    args = dill.loads(args)
                    result = func(args)
                else:
                    result = func()
            except Exception as ex:
                result_exception = ex
        elif isinstance(payload, dict) and payload.get('type') == 'python_isolated':
            if stdin:
                errors.append('stdin is not supported for this payload type!')
            cmd = sys.executable + ' isolated_worker_helper.py ' + str(update_key)
            print('Running isolated cmd:', cmd)
            stdin = dill.dumps(CONNECTION_DETAILS)
        else:
            errors.append('Unsupported payload type!')
      
      if errors:
        async with __pg_lock:
          fetch_result = await conn.fetchval('SELECT update_task_result($1, $2::json, FALSE)', update_key, json.dumps(errors))
        async with __pg_lock:
          return_result = await conn.fetchval('SELECT return_task($1)', update_key)
        print('The task', update_key, 'in slot', task_index, 'was returned, DB Update Result:', update_key, return_result)
      else:
        if False:
          await wait_test()
        
        add_result = False
        if cmd:
          retcode, stdout, stderr = await run_shell_cmd(cmd, stdin)
          task_result = {'retcode': retcode, 'stdout': base64.b85encode(stdout).decode('ascii'), 'stderr': base64.b85encode(stderr).decode('ascii')}
          
          if isinstance(payload, dict) and payload.get('type') == 'python_isolated':
             async with __pg_lock:
                result = await conn.fetchval('SELECT result FROM tasks WHERE update_key=$1', update_key)
                if result:
                    result = dill.loads(base64.b85decode(json.loads(result)))
                    add_result = True
        else:
          task_result = {}
          add_result = True
        
        if add_result:          
          if result_exception:
            task_result['exception'] = str(result_exception)
          elif isinstance(result, int):
            task_result['int'] = result
          elif isinstance(result, str):
            task_result['str'] = result
          elif isinstance(result, bool):
            task_result['bool'] = result
          elif isinstance(result, bytes):
            task_result['bytes'] = base64.b85encode(result).decode('ascii')
          else:
            try:
              task_result['object'] = base64.b85encode(dill.dumps(result)).decode('ascii')
            except Exception as ex:
              task_result['exception'] = str(ex)
        
        async with __pg_lock:
            fetch_result = await conn.fetchval('SELECT update_task_result($1, $2::json)', update_key, json.dumps(task_result))
        print('The task', update_key, 'in slot', task_index, 'finished, DB Update Result:', update_key, fetch_result)
    except Exception as ex:
      print('The task', update_key, 'in slot', task_index, 'resulted in an exception:', ex)
    finally:
      __threads[task_index] = None
    
    await fetch_a_task(conn)

def test_requirements(requirements):
    errors = []
    if isinstance(requirements, dict):
        print('Checking the requirements', requirements)
        for file in requirements.get('files', []):
            if not (isinstance(file, str) and os.path.isfile(file)):
                errors.append('The file <' + file + '> does not exist!')
        for directory in requirements.get('directories', []):
            if not (isinstance(directory, str) and os.path.isdir(directory)):
                errors.append('The directory <' + directory + '> does not exist!')
    elif requirements:
       errors.append('Unable to check the requirements!')
    return errors

async def wait_test():
          from time import sleep, time
          from random import randint
          delay = randint(1, 8)
          start = time()
          await asyncio.sleep(delay)
          print('Waited', time() - start, 'seconds instead of', delay)

async def run_shell_cmd(cmd, stdin):
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdin=asyncio.subprocess.PIPE if stdin else None,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)
    
    stdout, stderr = await proc.communicate(input=stdin or None)
    if PRINT_SUBPROCESS_RESULTS:
      print(stdout, stderr)
    
    return proc.returncode, stdout, stderr

def get_platform_id():
    platform_id = 0

    if platform.system() == 'Windows':
        platform_id += 1
    if platform.system() == 'Darwin':
        platform_id += 2
    if platform.system() == 'Linux':
        platform_id += 4

    if is_admin():
        platform_id += 256
    if platform.system() == 'Windows':
        platform_id += 512
    return platform_id

def is_admin():
    try:
       return os.getuid() == 0
    except AttributeError:
       return ctypes.windll.shell32.IsUserAnAdmin() != 0

if __name__ == '__main__':
  exit(main())

