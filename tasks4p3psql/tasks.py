import os
import sys
import json
import base64
import asyncio

import dill
import asyncpg

from task_types import *

CONNECTION_DETAILS = {'user':'postgres', 'password':'', 'database':'tasks', 'host':'194.62.99.21', 'port':'8880'}

__pg_lock = asyncio.Lock()
__main_lock = asyncio.Lock()
__main_task = None
__tasks = []
#__other_tasks = []

def main():
    from time import time
    tasks = [ShellTask('find /')]
    tasks = [EvalTask('2*' + str(i)) for i in range(1000+1)]
    tasks = [FunctionTask(lambda : 2 * i) for i in range(1000+1)]
    tasks = [SafeFunctionTask(lambda : 2 * i) for i in range(1000+1)]
    start = time()
    for task in run_tasks(tasks):
        pass
        #print(task.input, '=', task.result)
        #print(task.input, '=', task.stdout.decode('utf-8'))
    print(time() - start, len(tasks))

def run_tasks(tasks):
    global __main_task
    
    __tasks.clear()
    __tasks.extend( tasks )
    
    loop = asyncio.get_event_loop()
    __main_task = loop.create_task(asyncpg_listen(CONNECTION_DETAILS, 'updated_task', on_notification_handler, on_connection_handler))
    try:
        loop.run_until_complete(__main_task)
    except asyncio.CancelledError:
        pass#print('Tasks remaining', len(asyncio.tasks.all_tasks(loop)))
    except KeyboardInterrupt:
        print()
    
    for task in tasks:
       if task.is_finished:
         yield task

async def asyncpg_listen(
    connection_details, channel, on_notification_handler, on_connection_handler=None, *, conn_check_interval=60, conn_check_timeout=5, reconnect_delay=1):
    
    conn = None
    try:
      while True:
        try:
            conn = await asyncpg.connect(**connection_details)
            await conn.add_listener(channel, on_notification_handler)
            
            if on_connection_handler is not None:
                await on_connection_handler(conn)
            
            while True:
                await asyncio.sleep(conn_check_interval)
                async with __pg_lock:
                  await conn.execute('select 1', timeout=conn_check_timeout)
        except asyncio.CancelledError:
            #global __other_tasks
            #print('Cancellation requested...', len(__other_tasks), len([x for x in __other_tasks if not x.done()]))
            #for other_task in __other_tasks:
            #    other_task.cancel()
            #    await other_task
            #print('Cancelled...', len(__other_tasks))
            #__other_tasks.clear()
            raise
        except KeyboardInterrupt:
            raise
        except:
            # Probably lost connection
            pass
        
        if reconnect_delay > 0:
            await asyncio.sleep(reconnect_delay)
    finally:
      try:
        if conn != None:
          await conn.close()
      except:
        pass

def on_notification_handler(conn, pid, channel, payload):
     #print('Notify me, payload', payload, len(__other_tasks))
     task = asyncio.create_task(handle_updated_task(conn, int(payload)))
     #__other_tasks.append( task )

async def on_connection_handler(conn):
     async with __main_lock:
         for task in __tasks:
             if not task.is_finished:
               task.task_id = await add_task(conn, task)

async def handle_updated_task(conn, task_id):
   async with __main_lock:
      task = [task for task in __tasks if task.task_id == task_id and not task.is_finished]
      if not task:
        #print('Ignoring an update on task', task_id)
        return
      
      async with __pg_lock:
          fetch_result = await conn.fetchrow('SELECT result FROM tasks WHERE id=$1 AND is_finished=TRUE', task_id)
      if fetch_result:
            result = json.loads(fetch_result['result'] or '{}')
            task[0].is_finished = True
            if 'int' in result:
              task[0].result = result['int']
            else:
              task[0].result = result
            task[0].stdout = base64.b85decode(result.get('stdout') or b'')
            task[0].stderr = base64.b85decode(result.get('stderr') or b'')
      
      for task in __tasks:
        if not task.is_finished:
          #print('Still waiting for task', task.task_id)
          break
      else:
        #print('No more tasks!')
        __main_task.cancel()
        #raise KeyboardInterrupt

async def add_task(conn, task):
  name = None
  async with __pg_lock:
    fetch_result = await conn.fetchval('SELECT register_task($1, $2::json, NULL)', name, json.dumps(task.payload))
  #print('Task was added:', fetch_result)
  return fetch_result

if __name__ == '__main__':
  main()

