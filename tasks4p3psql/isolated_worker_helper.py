import sys
import json
import base64
import asyncio

import dill
import asyncpg

async def execute_task(connection_details, update_key):
    conn = await asyncpg.connect(**connection_details)
    payload = await conn.fetchval('SELECT payload FROM tasks WHERE update_key=$1::uuid', update_key)
    if not payload:
        return

    payload = json.loads(payload)
    func, args = str(payload.get('func', '')), str(payload.get('args', ''))
    func = base64.b85decode(func)
    args = base64.b85decode(args)
    func = dill.loads(func)
    if args:
                    args = dill.loads(args)
                    result = func(args)
    else:
                    result = func()

    task_result = json.dumps(base64.b85encode(dill.dumps(result)).decode('ascii'))
    fetch_result = await conn.fetchval('SELECT update_task_result($1::uuid, $2::json, FALSE)', update_key, task_result)

def main():
    details = read_stdin()
    if not details:
        return 1
    details = dill.loads(details)
    update_key = dict(enumerate(sys.argv))[1]
    loop = asyncio.get_event_loop()
    task = loop.create_task(execute_task(details, update_key))
    try:
        loop.run_until_complete(task)
    except KeyboardInterrupt:
        print()
    finally:
        task.cancel()
    return 0

def read_stdin():
    if is_stdin_terminal():
        return None
    data = sys.stdin.buffer.read()
    return data

def is_stdin_terminal():
    return sys.stdin.isatty()

if __name__ == '__main__':
  exit(main())

