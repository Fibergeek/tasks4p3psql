# SOURCE: https://mike.depalatis.net/blog/simplifying-argparse.html
import argparse

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="subcommand")

def main():
    args = parser.parse_args()
    if args.subcommand is None:
        return print_help()
    return args.func(args)

def print_help():
    parser.print_help()
    return 0

def subcommand(args=[], name=None, parent=subparsers):
    def decorator(func):
        parser = parent.add_parser(name if name else func.__name__, description=func.__doc__)
        for arg in args:
            parser.add_argument(*arg[0], **arg[1])
        parser.set_defaults(func=func)
    return decorator

def argument(*name_or_flags, **kwargs):
    return list(name_or_flags), kwargs
