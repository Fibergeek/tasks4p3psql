import sys
import argparse

from .worker import main as worker_main
from .worker import test as worker_test
from .worker import print_info as worker_print_ns

from .subcommand import subcommand, argument, print_help, main

ARG_wid = [argument('--wid', type=str, default='', help='Override the ID (GUID) of the worker')]
ARG_cpu_count = [argument('--cpu_count', '-c', type=int, default=0, help='Override the CPU count for the worker')]
ARG_host = [argument('--host', '-H', type=str, default='localhost', help='Hostname or IP address of the server ("localhost"')]
ARG_port = [argument('--port', '-P', type=int, default=5432, help='Port of the server (5432)')]
ARG_user = [argument('--username', '-U', type=str, default='postgres', help='Username to use ("postgres")')]
ARG_pass = [argument('--password', '-W', type=str, default='', help='Password to use ("")')]
ARG_dbname = [argument('--dbname', '-D', type=str, default='tasks', help='Database to use ("tasks")')]
ARG_sslcrt = [argument('--sslcrt', '-C', type=str, default='', help='Filename of CRT file to use (this will force SSL and also requires --sslkey)')]
ARG_sslkey = [argument('--sslkey', '-K', type=str, default='', help='Filename of KEY file to use (this will force SSL and also requires --sslcrt)')]
ARGS_sql = ARG_host + ARG_port + ARG_user + ARG_pass + ARG_dbname + ARG_sslcrt + ARG_sslkey

@subcommand()
def help(args):
    return print_help()

@subcommand(ARG_wid + ARG_cpu_count)
def ns(args):
    return worker_print_ns(args)

@subcommand(ARG_wid + ARG_cpu_count + ARGS_sql) 
def test(args):
    return worker_test(args)

@subcommand(ARG_wid + ARG_cpu_count + ARGS_sql)
def worker(args):
    return worker_main(args)

if __name__ == '__main__':
    exit(main())
