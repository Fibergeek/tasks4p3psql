--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3 (Ubuntu 13.3-1.pgdg20.04+1)
-- Dumped by pg_dump version 13.2

-- Started on 2021-05-31 21:46:22 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 226 (class 1255 OID 16385)
-- Name: cancel_task(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.cancel_task(_task_id integer) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _updated_id integer;
BEGIN
  UPDATE "tasks"
    SET "result"='{}'::json,
	    "update_key"=NULL,
	    "is_finished"=TRUE,
		"update_ts"=NOW()
	WHERE "id"=_task_id
	  AND "is_finished"=FALSE
	RETURNING "id"
	INTO _updated_id;
  
  IF _updated_id IS NOT NULL THEN
    PERFORM pg_notify('updated_task', _updated_id::text);
  END IF;
  
  RETURN _updated_id IS NOT NULL;
END;
$$;


ALTER FUNCTION public.cancel_task(_task_id integer) OWNER TO postgres;

--
-- TOC entry 208 (class 1255 OID 16386)
-- Name: convert_to_int(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.convert_to_int(_text text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
   IF _text = '' THEN  -- special case for empty string like requested
      RETURN 0;
   ELSE
      RETURN _text::int;
   END IF;

EXCEPTION WHEN OTHERS THEN
   RETURN NULL;  -- NULL for other invalid input

END
$$;


ALTER FUNCTION public.convert_to_int(_text text) OWNER TO postgres;

--
-- TOC entry 225 (class 1255 OID 16387)
-- Name: get_available_task(uuid, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_available_task(_worker_key uuid, _available_platform integer) RETURNS uuid
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _worker_id integer;
  _task_id integer;
  _update_key uuid;
BEGIN
  SELECT "id"
    FROM workers
	WHERE "public_id" = _worker_key
	INTO _worker_id;
  IF NOT FOUND THEN
    RETURN NULL;
  END IF;
  UPDATE workers
    SET "lastseen_ts"=NOW()
	WHERE "id"=_worker_id;    

  SELECT "id"
    FROM tasks
	WHERE "update_key" IS NULL AND
	      "is_finished"=false AND
		  (COALESCE(convert_to_int("requirements"->>'platform'),0)=0 OR
			  (COALESCE(convert_to_int("requirements"->>'platform'),0)&_available_platform)=_available_platform) AND
	  ("banned_workers" IS NULL OR NOT _worker_id = ANY("banned_workers"))
	ORDER BY "id"
	LIMIT 1
    INTO _task_id;
  IF FOUND THEN
    SELECT md5(random()::text || clock_timestamp()::text)::uuid
	  INTO _update_key;
    UPDATE "tasks"
	  SET "result"=NULL,
	      "assigned_ts"=NOW(),
		  "worker_id"=_worker_id,
	      "update_key"=_update_key,
		  "update_ts"=NOW()
      WHERE "id" = _task_id;
    RETURN _update_key;
  END IF;
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.get_available_task(_worker_key uuid, _available_platform integer) OWNER TO postgres;

--
-- TOC entry 210 (class 1255 OID 16388)
-- Name: is_updatekey_valid(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.is_updatekey_valid(_update_key uuid) RETURNS boolean
    LANGUAGE plpgsql STABLE SECURITY DEFINER PARALLEL SAFE
    AS $$
DECLARE
  _count integer;
BEGIN
  SELECT COUNT(*)
    FROM "tasks"
	WHERE "update_key"=_update_key
	INTO _count;
  RETURN _count > 0;
END;
$$;


ALTER FUNCTION public.is_updatekey_valid(_update_key uuid) OWNER TO postgres;

--
-- TOC entry 209 (class 1255 OID 16389)
-- Name: register_task(text, json, json); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.register_task(_name text, _payload json, _requirements json) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER PARALLEL RESTRICTED
    AS $$
DECLARE 
  return_id integer;
BEGIN
  INSERT INTO tasks ("name", "payload", "requirements")
    VALUES (_name,_payload,_requirements)
	RETURNING "id"
	INTO return_id;
	PERFORM pg_notify('new_task', return_id::text);
    RETURN return_id;
  EXCEPTION
  WHEN UNIQUE_VIOLATION THEN
    SELECT "id" AS return_id
	  FROM tasks
	  WHERE "name"=_name
	  INTO return_id;
  RETURN return_id;
END;
$$;


ALTER FUNCTION public.register_task(_name text, _payload json, _requirements json) OWNER TO postgres;

--
-- TOC entry 223 (class 1255 OID 16390)
-- Name: register_worker(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.register_worker(_public_id uuid) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
   _update_key uuid;
   _task_id integer;
   _id integer;
BEGIN
  FOR _update_key IN 
    SELECT "update_key"
      FROM tasks
  	  WHERE "update_key" IS NOT NULL
	    AND "is_finished"=false
	    AND NOW()-"update_ts">make_interval(secs=>"timeout")
  LOOP
    UPDATE tasks
    SET "update_key"=NULL,
		"update_ts"=NOW()
	WHERE "update_key"=_update_key
	RETURNING "id"
	INTO _task_id;

    IF _task_id IS NOT NULL THEN
      PERFORM pg_notify('new_task', _task_id::text);
    END IF;
  END LOOP;

  -- We use a select to avoid increasing the sequence table --
  -- This is slower, but I don't want to worry about it     --
  SELECT "id"
    FROM workers
    WHERE "public_id" = _public_id
    INTO _id;
  IF NOT FOUND THEN
    -- Insert but use a fallback ON CONFLICT                       --
	-- If a conflict happens, then 2 clients are using the same ID --
    INSERT INTO workers 
      ("public_id")
      VALUES (_public_id)
      ON CONFLICT ("public_id")
      DO UPDATE SET "lastseen_ts" = NOW()
	  RETURNING "id"
	  INTO _id;
  ELSE
    UPDATE workers
      SET "lastseen_ts"=NOW()
	  WHERE "id"=_id;    
  END IF;

  RETURN _id;
END;
$$;


ALTER FUNCTION public.register_worker(_public_id uuid) OWNER TO postgres;

--
-- TOC entry 222 (class 1255 OID 16391)
-- Name: return_task(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.return_task(_update_key uuid) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _worker_id integer;
  _task_id integer;
BEGIN
  SELECT "worker_id"
    FROM "tasks"
	WHERE "update_key"=_update_key
	INTO _worker_id;
  IF NOT FOUND THEN
    RETURN false;
  END IF;
  
  UPDATE tasks
    SET "banned_workers"=array_append("banned_workers", _worker_id),
	    "update_key"=NULL,
		"update_ts"=NOW()
	WHERE "update_key"=_update_key
	RETURNING "id"
	INTO _task_id;

  IF _task_id IS NOT NULL THEN
    PERFORM pg_notify('new_task', _task_id::text);
  END IF;
    
  RETURN true;
END;
$$;


ALTER FUNCTION public.return_task(_update_key uuid) OWNER TO postgres;

--
-- TOC entry 224 (class 1255 OID 16392)
-- Name: update_task_result(uuid, json, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.update_task_result(_update_key uuid, _result json, _is_finished boolean DEFAULT true) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _updated_id integer;
BEGIN
  UPDATE "tasks"
    SET "result"=_result,
	    "is_finished"=_is_finished,
	    "update_key"=(CASE WHEN _is_finished THEN NULL ELSE _update_key END),
		"update_ts"=NOW()
	WHERE "update_key"=_update_key
	RETURNING "id"
	INTO _updated_id;

  IF _updated_id IS NOT NULL THEN
    PERFORM pg_notify('updated_task', _updated_id::text);
  END IF;
  
  RETURN _updated_id IS NOT NULL;
END;
$$;


ALTER FUNCTION public.update_task_result(_update_key uuid, _result json, _is_finished boolean) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 200 (class 1259 OID 16393)
-- Name: tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tasks (
    id integer NOT NULL,
    name text,
    requirements json,
    payload json NOT NULL,
    result json,
    insert_ts timestamp without time zone DEFAULT now() NOT NULL,
    update_ts timestamp without time zone,
    update_key uuid,
    banned_workers integer[],
    worker_id integer,
    is_finished boolean DEFAULT false NOT NULL,
    timeout integer DEFAULT 60 NOT NULL,
    assigned_ts timestamp without time zone,
    dependant_task_ids integer[] NOT NULL
);


ALTER TABLE public.tasks OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16447)
-- Name: active_tasks; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.active_tasks WITH (security_barrier='true') AS
 SELECT tasks.id,
    tasks.name,
    tasks.requirements,
    tasks.payload,
    tasks.result,
    tasks.insert_ts,
    tasks.update_ts,
    tasks.update_key,
    tasks.banned_workers,
    tasks.worker_id,
    tasks.is_finished,
    tasks.timeout,
    tasks.assigned_ts,
    tasks.dependant_task_ids
   FROM public.tasks
  WHERE (tasks.is_finished = false);


ALTER TABLE public.active_tasks OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16404)
-- Name: workers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workers (
    id integer NOT NULL,
    lastseen_ts timestamp without time zone DEFAULT now() NOT NULL,
    public_id uuid NOT NULL,
    firstseen_ts timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.workers OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16437)
-- Name: active_workers; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.active_workers WITH (security_barrier='true') AS
 SELECT workers.public_id
   FROM public.workers
  WHERE ((now() - (workers.lastseen_ts)::timestamp with time zone) > '00:30:00'::interval);


ALTER TABLE public.active_workers OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16441)
-- Name: all_workers; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.all_workers WITH (security_barrier='true') AS
 SELECT workers.public_id,
    workers.lastseen_ts
   FROM public.workers;


ALTER TABLE public.all_workers OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16451)
-- Name: running_tasks; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.running_tasks WITH (security_barrier='true') AS
 SELECT tasks.id,
    tasks.name,
    tasks.requirements,
    tasks.payload,
    tasks.result,
    tasks.insert_ts,
    tasks.update_ts,
    tasks.update_key,
    tasks.banned_workers,
    tasks.worker_id,
    tasks.is_finished,
    tasks.timeout,
    tasks.assigned_ts,
    tasks.dependant_task_ids
   FROM public.tasks
  WHERE ((tasks.is_finished = false) AND (tasks.worker_id IS NOT NULL));


ALTER TABLE public.running_tasks OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16402)
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tasks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_id_seq OWNER TO postgres;

--
-- TOC entry 3019 (class 0 OID 0)
-- Dependencies: 201
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tasks_id_seq OWNED BY public.tasks.id;


--
-- TOC entry 203 (class 1259 OID 16407)
-- Name: workers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.workers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.workers_id_seq OWNER TO postgres;

--
-- TOC entry 3020 (class 0 OID 0)
-- Dependencies: 203
-- Name: workers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.workers_id_seq OWNED BY public.workers.id;


--
-- TOC entry 2856 (class 2604 OID 16409)
-- Name: tasks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks ALTER COLUMN id SET DEFAULT nextval('public.tasks_id_seq'::regclass);


--
-- TOC entry 2857 (class 2604 OID 16410)
-- Name: workers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers ALTER COLUMN id SET DEFAULT nextval('public.workers_id_seq'::regclass);


--
-- TOC entry 2863 (class 2606 OID 16412)
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- TOC entry 2866 (class 2606 OID 16421)
-- Name: workers worker_id_must_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT worker_id_must_unique UNIQUE (public_id);


--
-- TOC entry 2868 (class 2606 OID 16414)
-- Name: workers workers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_pkey PRIMARY KEY (id);


--
-- TOC entry 2860 (class 1259 OID 16415)
-- Name: is_finished_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX is_finished_index ON public.tasks USING btree (is_finished);


--
-- TOC entry 2861 (class 1259 OID 16416)
-- Name: taskname_is_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX taskname_is_unique ON public.tasks USING btree (name COLLATE "C.UTF-8") WHERE (name IS NOT NULL);


--
-- TOC entry 2864 (class 1259 OID 16417)
-- Name: updatekey_is_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX updatekey_is_unique ON public.tasks USING btree (update_key) WHERE (update_key IS NOT NULL);


--
-- TOC entry 3008 (class 0 OID 0)
-- Dependencies: 226
-- Name: FUNCTION cancel_task(_task_id integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION public.cancel_task(_task_id integer) FROM PUBLIC;
GRANT ALL ON FUNCTION public.cancel_task(_task_id integer) TO tasks4p3psql_admins;
GRANT ALL ON FUNCTION public.cancel_task(_task_id integer) TO tasks4p3psql_users;


--
-- TOC entry 3009 (class 0 OID 0)
-- Dependencies: 225
-- Name: FUNCTION get_available_task(_worker_key uuid, _available_platform integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION public.get_available_task(_worker_key uuid, _available_platform integer) FROM PUBLIC;
GRANT ALL ON FUNCTION public.get_available_task(_worker_key uuid, _available_platform integer) TO tasks4p3psql_workers;


--
-- TOC entry 3010 (class 0 OID 0)
-- Dependencies: 210
-- Name: FUNCTION is_updatekey_valid(_update_key uuid); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION public.is_updatekey_valid(_update_key uuid) FROM PUBLIC;
GRANT ALL ON FUNCTION public.is_updatekey_valid(_update_key uuid) TO tasks4p3psql_workers;


--
-- TOC entry 3011 (class 0 OID 0)
-- Dependencies: 209
-- Name: FUNCTION register_task(_name text, _payload json, _requirements json); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION public.register_task(_name text, _payload json, _requirements json) FROM PUBLIC;
GRANT ALL ON FUNCTION public.register_task(_name text, _payload json, _requirements json) TO tasks4p3psql_users;


--
-- TOC entry 3012 (class 0 OID 0)
-- Dependencies: 223
-- Name: FUNCTION register_worker(_public_id uuid); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION public.register_worker(_public_id uuid) FROM PUBLIC;
GRANT ALL ON FUNCTION public.register_worker(_public_id uuid) TO tasks4p3psql_workers;


--
-- TOC entry 3013 (class 0 OID 0)
-- Dependencies: 222
-- Name: FUNCTION return_task(_update_key uuid); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION public.return_task(_update_key uuid) FROM PUBLIC;
GRANT ALL ON FUNCTION public.return_task(_update_key uuid) TO tasks4p3psql_users;


--
-- TOC entry 3014 (class 0 OID 0)
-- Dependencies: 224
-- Name: FUNCTION update_task_result(_update_key uuid, _result json, _is_finished boolean); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION public.update_task_result(_update_key uuid, _result json, _is_finished boolean) FROM PUBLIC;
GRANT ALL ON FUNCTION public.update_task_result(_update_key uuid, _result json, _is_finished boolean) TO tasks4p3psql_workers;


--
-- TOC entry 3015 (class 0 OID 0)
-- Dependencies: 206
-- Name: TABLE active_tasks; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.active_tasks TO tasks4p3psql_admins;


--
-- TOC entry 3016 (class 0 OID 0)
-- Dependencies: 204
-- Name: TABLE active_workers; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.active_workers TO tasks4p3psql_admins;


--
-- TOC entry 3017 (class 0 OID 0)
-- Dependencies: 205
-- Name: TABLE all_workers; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.all_workers TO tasks4p3psql_admins;


--
-- TOC entry 3018 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE running_tasks; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.running_tasks TO tasks4p3psql_admins;


-- Completed on 2021-05-31 21:46:28 CEST

--
-- PostgreSQL database dump complete
--

