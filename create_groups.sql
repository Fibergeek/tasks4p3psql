SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DO  
$body$
BEGIN
  CREATE GROUP tasks4p3psql_admins;
  EXCEPTION WHEN DUPLICATE_OBJECT THEN
  RAISE NOTICE 'Not creating group tasks4p3psql_admins as it already exists.';
END
$body$;

DO  
$body$
BEGIN
  CREATE GROUP tasks4p3psql_users;
  EXCEPTION WHEN DUPLICATE_OBJECT THEN
  RAISE NOTICE 'Not creating group tasks4p3psql_users as it already exists.';
END
$body$;

DO  
$body$
BEGIN
  CREATE GROUP tasks4p3psql_workers;
  EXCEPTION WHEN DUPLICATE_OBJECT THEN
  RAISE NOTICE 'Not creating group tasks4p3psql_workers as it already exists.';
END
$body$;
